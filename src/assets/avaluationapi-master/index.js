const express = require("express"); // es el nostre servidor web
const jwt = require("jsonwebtoken");
const cors = require('cors'); // ens habilita el cors recordes el bicing???
const bodyParser = require('body-parser'); // per a poder rebre jsons en el body de la resposta
const app = express();
const baseUrl = '/miapi';
app.use(cors());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.use(bodyParser.urlencoded({ extended: false })); //per a poder rebre json en el reuest
app.use(bodyParser.json());

//La configuració de la meva bbdd

//El pool es un congunt de conexions
//const Pool = require('pg').Pool
//const pool = new Pool(ddbbConfig);

var mysql = require('mysql');
var connection = mysql.createConnection({
    user: '234185',
    host: 'mysql-avaluation.alwaysdata.net',
    database: 'avaluation_bbdd',
    password: 'ProjecteAvaluation',
    port: 3306
});

//Exemple endPoint
//Quan accedint a http://localhost:3000/miapi/test   ens saludará
const getAlumnos = (request, response) => {

    connection.connect();

    var consulta = "SELECT * FROM Alumno"

    connection.query(consulta, function (err, rows, fields) {
        if (err) throw err;
        response.status(200).json(rows)
    });

    connection.end();
}

app.get(baseUrl + '/getAlumnos', getAlumnos);

const getLogin = (request, response) => {

    connection.connect();

    console.log(request.body);
    const {dni,pass}=request.body;

    //Si el DNI y/o PASSWORD introducidos están vacíos, undefined o null gestiono el error
    if (dni == "" || dni == null || typeof dni === 'undefined' || pass == "" || pass == null || typeof dni === 'undefined'){
        response.status(401).json({
            message: "ERROR"
        })
    }

    var consulta = `SELECT pass_alumno FROM Alumno WHERE dni_alumno='${dni}'`;
    let user_pass;

    connection.query(consulta, function (err, rows, fields) {

        //Este if gestiona errores con la base de datos
        if (err){
            console.log("Se ha producido un error SQL");

        //Realiza la query en caso que no haya problemas con la base de datos
        }else{
            
            //Este if mira si la consulta SQL devuelve un undefined y muestra el error 401
            if(rows[0] == null){
                response.status(401).json({
                    message: "ERROR"
                })
            }

            //Si la consulta devuelve resultados se hace la comprovación de que el password recibido corresponda con el introducido
            else{
                user_pass=rows[0].pass_alumno;

                //Si el password recibido se corresponde con el introducido devuelve un mensaje: "OK" y un token de acceso
                if (user_pass == pass){
                    const user = {
                        nombre: dni
                    }
                    jwt.sign({user: user}, 'secretKey',{expiresIn: '32s'}, (err, token) => {
                        response.status(200).json({
                            message: "OK",
                            token
                        })
                    })

                //Si el password no se corresponde con el introducido devuelve un mensaje: "ERROR"
                }else{
                    response.status(401).json({
                        message: "ERROR"
                    })
                }
            }
        }
    });
    //connection.end();
}

app.post(baseUrl + '/Login', getLogin);

const getModulos = (request, response) => {

    //connection.connect();

    const {dni}=request.body;
    console.log(request.body)
    console.log(dni);

    var consulta = `SELECT * FROM Modulo WHERE dni_professor = '${dni}'`;

    connection.query(consulta, function (err, rows, fields) {
        console.log(consulta);
        if (err) throw error;
        console.log(rows);
        response.status(200).json(rows)
    });

    connection.end();
}

app.post(baseUrl + '/getModulos', getModulos);

/*app.post(baseUrl+'/loginuser', (req, res) => {
    const user = {
        id:1,
        nombre: "Facu",
        email: "facu@email.com"
    }

    jwt.sign({user: user}, 'secretKey',{expiresIn: '32s'}, (err, token) => {
        res.json({
            token
        })
    })


});*/

app.post(baseUrl+'/posts',verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretKey', (err, authData)=> {
        if(err){
            res.sendStatus(403);
        }else{
            res.json({
                mensaje: "Post fue creado",
                authData
            });
        }
    })
});

//Authorization: Bearer <token>
function verifyToken(req,res,next){
    const bearerHeader = req.headers['authorization'];

    if (typeof bearerHeader !== 'undefined'){
        const bearerToken = bearerHeader.split(" ")[1];
        req.token = bearerToken;
        next();
    }else{
        res.sendStatus(403);
    }
}

//Inicialitzem el servei
const PORT = process.env.PORT || 3000; // Port
const IP = process.env.IP || null; // IP

app.listen(PORT, IP, () => {
    console.log("El servidor está inicialitzat en el puerto " + PORT);
});