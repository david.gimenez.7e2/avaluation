import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-cursos-professor',
  templateUrl: './cursos-professor.page.html',
  styleUrls: ['./cursos-professor.page.scss'],
})
export class CursosProfessorPage implements OnInit {

  classes:string[]=["SMX1A","SMX1B","SMX1C","SMX1D", "SMX1E", "SMX1F", "SMX2A", "SMX2B", "SMX2C", "ASIX1A", "ASIX1B", "ASIX1C", "ASIX1D", "ASIX2", "DAMi1A", "DAMi1B",
  "DAMi2A", "DAMi2B", "DAMv1A", "DAMv1B", "DAMv2A", "DAMv2B", "DAWe1A", "DAWe1B", "DAWe2A", "DAWe2B", "DAMr1A", "DAMr1B"];

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToClass(item) {
    let navigationExtras: NavigationExtras = {
      state: {
        parametres: item,
      }
    };
    this.router.navigate(['moduls-professor'], navigationExtras);

  }

}
