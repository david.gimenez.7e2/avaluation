import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CursosProfessorPageRoutingModule } from './cursos-professor-routing.module';

import { CursosProfessorPage } from './cursos-professor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CursosProfessorPageRoutingModule
  ],
  declarations: [CursosProfessorPage]
})
export class CursosProfessorPageModule {}
