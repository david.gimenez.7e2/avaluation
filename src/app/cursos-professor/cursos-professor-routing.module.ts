import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CursosProfessorPage } from './cursos-professor.page';

const routes: Routes = [
  {
    path: '',
    component: CursosProfessorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CursosProfessorPageRoutingModule {}
