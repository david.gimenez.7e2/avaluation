import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ApiserviceService } from '../services/apiservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  validar:boolean = false;
  showPassword = false;
  passwordToggleIcon = 'eye';

  info:any =[];

  //El usuario se inicializa vacío y se le asignan valor a los atributos dentro de la función login();
  user: any={
    dni: '',
    pass: ''
  }

  constructor(private router: Router, private apiService: ApiserviceService) { }

  ngOnInit() {
  }

  togglePassword(){
    this.showPassword = !this.showPassword
    if (this.passwordToggleIcon == 'eye') {
      this.passwordToggleIcon = 'eye-off';
    }else{
      this.passwordToggleIcon = 'eye';
    }
  }

  login(dni, password) {
    this.checkLogin(new usuario(dni, password));
  }

  async checkLogin(user){
    this.info= await this.apiService.getDni(user);
    //Login correcto
    if (this.info.message == "OK"){
      //Cargamos los modulos del profesor y los pasamos por NavigationExtras
      let navigationExtras: NavigationExtras = {
        state: {
          user: user,
        }
      };
      this.router.navigate(['moduls-professor'], navigationExtras);
    }
    //Login incorrecto
    else{
      //TODO
      console.log("Usuario o contraseña incorrectos")
    }
    }
  }

export class usuario{
  dni;
  pass;
constructor(dni, pass){
  this.dni = dni;
  this.pass = pass;
}
}