import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.page.html',
  styleUrls: ['./notes.page.scss'],
})
export class NotesPage implements OnInit {

  practiques:string[]=["P1","P2","P3","P4", "P5"];
  alumnes:any[];
  //practiques:string[]=["P01","P02","P03","P04", "P05", "P06", "P07", "P08", "P09", "P10", "P11", "P12", "P13", "P14"];

  constructor() { }

  ngOnInit() {
    fetch('src/assets/data/alumnes.json').then(res => res.json())
      .then(json => {
        this.alumnes = json;
      });
    console.log(this.alumnes.length);
  }

}
