import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiserviceService } from '../services/apiservice.service';

@Component({
  selector: 'app-moduls-professor',
  templateUrl: './moduls-professor.page.html',
  styleUrls: ['./moduls-professor.page.scss'],
})
export class ModulsProfessorPage implements OnInit {

  user : any;
  profesor: any = {
    dni: ""
  }
  //moduls:string[]=["M01","M02","M03","M04", "M05", "M06", "M07", "M08", "M09", "M10", "M11", "M12", "M13", "M14"];
  moduls:any[];

  constructor(private route: ActivatedRoute, private router: Router, private apiService: ApiserviceService) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.user = this.router.getCurrentNavigation().extras.state.user;
      }
    });
  
  }

  ngOnInit() {
    this.profesor.dni = this.user.dni;
    /*
    document.addEventListener('DOMContentLoaded', function() {
      this.getModulos(this.user);
    }, false);
    */
  }

  async getModulos(profesor:any) {
    this.moduls = await this.apiService.getModulos(profesor);
  }

}
